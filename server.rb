# frozen_string_literal: true
# encoding: utf-8

require 'sinatra'
require 'sinatra/base'

set :bind, '0.0.0.0'
set :run, true

get '/webhook' do
  Thread.current[:discordrb_name] = 'webhook'

  if params[:channel_id] && params[:message]
    BOT.send_message(params[:channel_id], params[:message])
  else
    BOT.user(JOHN_USER_ID).pm('Webhook, yay it works!')
  end

  200
end
