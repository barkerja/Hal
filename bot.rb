# frozen_string_literal: true
# encoding: utf-8

require 'rubygems'
require 'bundler/setup'
require 'json'

require 'discordrb'
require_relative './helpers/helpers'
require_relative './containers/overwatch'

NPC_SERVER_ID = 105713369939480576.freeze
JOHN_USER_ID = 105708217153126400.freeze

ADMINS = {
  JOHN_USER_ID => true
}.freeze

LASTPLAYED_CACHE = {}
LAST_ONLINE_CACHE = {}

BOT = Discordrb::Bot.new(
  application_id: ENV['APPLICATION_ID'],
  token: ENV['TOKEN'],
  name: ENV.fetch('NAME', 'Hal'),
  log_mode: ENV['LOG_MODE'].nil? ? nil : ENV['LOG_MODE'].to_sym,
  fancy_log: (ENV['FANCY_LOG'] == 'true')
)

BOT.include! DiscordBot::Containers::Overwatch

BOT.ready do |_event|
  @last_online ||= Time.now
  @reconnect_count ||= 0
  @reconnect_count += 1

  BOT.profile.avatar = File.open('hal.png')

  # If 10 minutes or more since last connection, notify John
  if (((@last_online - Time.now) / 60).round >= 10) || @reconnect_count == 1
    BOT.user(JOHN_USER_ID).pm('Heroes never die!')
    @last_online = Time.now

    BOT.user(JOHN_USER_ID).pm(
      %(This is reconnect #{@reconnect_count}. There may be an issue.)
    ) if @reconnect_count > 1
  end
end

BOT.playing do |event|
  next if event.game.nil?

  # if event.type == 1
  #   BOT.send_message(
  #     105713369939480576,
  #     "#{event.user.username} just started streaming #{event.game}: " \
  #     "#{event.url}"
  #   )
  # end

  now = Time.now.to_i

  # Don't broadcast game unless 60 seconds have passed for user since last
  # announcement.
  last_announcement = LASTPLAYED_CACHE[event.user.id]
  next if !last_announcement.nil? && (now - last_announcement) <= 60

  LASTPLAYED_CACHE[event.user.id] = now

  msg = "#{event.user.username} just started playing #{event.game}"
  BOT.send_message(168451278626750467, msg)
end

BOT.presence do |event|
  next unless event.server.id == NPC_SERVER_ID && event.status == :online

  now = Time.now.to_i
  last_online = LAST_ONLINE_CACHE[event.user.id]

  # Only indicate if new status change within last 15 minutes
  if last_online.nil? || ((now - last_online) >= 900)
    # user = BOT.user(JOHN_USER_ID)
    # user.pm("#{event.user.username} just came online")

    BOT.send_message(168451278626750467, "#{event.user.username} just came online.")

    LAST_ONLINE_CACHE[event.user.id] = now
  else
    last_online_in_minutes = ((now - last_online.to_i) / 60.0).round(2)

    puts "Ignoring presence update for #{event.user.username}"
    puts "Last online (minutes): #{last_online_in_minutes}"
  end
end

BOT.run :async
