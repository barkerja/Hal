# frozen_string_literal: true
# encoding: utf-8

require 'uri'
require 'net/http'
require 'json'

module DiscordBot
  module Containers
    module Overwatch
      extend Discordrb::EventContainer

      ALLOWED_ACTIONS = %w(counter counters).freeze
      HERO_JSON_GIST = 'https://gist.githubusercontent.com/barkerja/c24f505ae9a2da846709c2fa27c4b499/raw'.freeze

      def self.hero_command
        message(start_with: hero_command_regex) do |event|
          msg = hero_command_regex.match(event.content.downcase)

          if msg.nil?
            event.respond 'There was an issue with that command.'
            next
          end

          hero = msg[1]&.downcase
          action = msg[2]&.downcase&.strip

          if hero.nil?
            event.respond 'There was an issue. Blame John (probably bad hero)'
            event.respond 'Hero options are: '
            event.respond @heros.join(', ')
            next
          end

          counters = self.hero_counters(msg[1])

          case action
          when 'counter'
            event.respond '%s counters: %s' % [counters.join(', '), hero]
          when 'counters'
            event.respond '%s counters: %s' % [hero, self.get_json[hero].join(', ')]
          else 
            event.respond '%s counters: %s' % [counters.join(', '), hero]
            event.respond '%s counters: %s' % [hero, self.get_json[hero].join(', ')]
          end
        end
      end

      message(start_with: /^!ow reload json$/) do |event|
        self.reload_json
        event.respond 'Overwatch Hero JSON reloaded successfully'
        event.respond 'Heros I know about: '
        event.respond @heros.join(', ')
      end

      def self.get_json
        @json ||= begin
          uri = URI.parse(HERO_JSON_GIST)
          JSON.parse(Net::HTTP.get_response(uri).body)
        end

        @json
      end

      def self.reload_json
        @json = nil
        @heros = self.get_json.keys.map(&:downcase)

        # Initialize/Reload hero command
        hero_command
      end

      def self.hero_command_regex
        Regexp.compile(
          '^!(' + @heros.join('|') + ')(\scounters?)?$',
          Regexp::IGNORECASE
        )
      end

      # @return Array
      def self.hero_counters(hero_counter)
        matches = Set.new

        self.get_json.each do |hero, counters|
          matches << hero if counters.include?(hero_counter)
        end

        matches.to_a
      end

      # Initialize JSON from remote source
      reload_json
    end
  end
end
