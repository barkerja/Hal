FROM ruby:2.3-alpine
MAINTAINER "John Barker <jabarker1@gmail.com>"

RUN apk upgrade -U && apk add ca-certificates git libffi-dev ffmpeg libsodium-dev opus-dev build-base && rm -rf /var/cache/*
RUN gem install bundler
ADD Gemfile* /bot/

ENV BUNDLE_GEMFILE=/bot/Gemfile \
    BUNDLE_PATH=/bundle

RUN bundle install

ADD . /bot
WORKDIR /bot

# What port should Sinatra listen on?
ENV PORT 3000

CMD ["ruby",  "bin/run"]
