# frozen_string_literal: true
# encoding: utf-8

def set_status(bot, status = 'Music ♫')
  bot.game = status
end

def play_url(event, url = nil, status = 'Music ♫')
  if !event.user.voice_channel
    event.respond 'You must be in a voice channel'
    return false
  end

  if !url
    event.respond 'You must define a URL to stream'
    return false
  end

  event.respond "Playing #{(status || url)}"

  vc = event.bot.voice_connect(event.user.voice_channel)
  vc.length_override = 16.7
  vc.encoder.bitrate = 96_000

  begin
    set_status event.bot, status
    vc.play_file(url)
  rescue
    event.respond 'Error playing that requested URL'
    set_status event.bot, ''
    vc.destroy
  end
end
